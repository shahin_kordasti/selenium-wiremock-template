package com.gu.fronts.integration.test.common;

import org.junit.After;
import org.junit.BeforeClass;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import com.gu.fronts.integration.test.config.EnvironmentConfigurer;
import com.gu.fronts.integration.test.page.nwfront.NetworkFrontPage;
import com.gu.fronts.integration.test.page.util.CustomPageFactory;

/**
 * This is the super class of all integration test classes. It contains convenience method to get the start (network
 * front page)
 */
public class FrontsIntegrationTestCase {

    @Autowired
    @Qualifier("firefox")
    protected WebDriver webDriver;
    @Value("${fronts.base.url}")
    private String frontsBaseUrl;
    @Autowired
    protected CustomPageFactory pageFactoryHelper;

    protected NetworkFrontPage networkFrontPage;

    @BeforeClass
    public static void testClassSetup() {
        EnvironmentConfigurer.setupEnvironmentProperty();
    }

    @After
    public void cleanSlate() {
        webDriver.manage().deleteAllCookies();
        webDriver.quit();
    }

    protected NetworkFrontPage openNetworkFrontPage() {
        // need to open a page of the same domain because selenium need to first go to a page before setting the cookie
        webDriver.get(frontsBaseUrl + "/pagenotfound");
        webDriver.manage().addCookie(new Cookie("GU_VIEW", "responsive"));
        webDriver.get(frontsBaseUrl);
        return networkFrontPage();
    }

    protected NetworkFrontPage networkFrontPage() {
        return pageFactoryHelper.initPage(webDriver, NetworkFrontPage.class);
    }
}
