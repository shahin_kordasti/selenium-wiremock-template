package com.gu.fronts.integration.test.categories;

/**
 * This test category is used to indicate that the response(s) from external systems, such as AWS and Content-API, are
 * stubbed.
 */
public class Stubbed {

}
